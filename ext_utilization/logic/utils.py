from datetime import datetime, timezone
from os.path import join, getmtime

__all__ = [
    'get_file_modified_time',
]


def get_file_modified_time(path, file):
    return datetime.fromtimestamp(
        getmtime(join(path, file)),
        timezone.utc
    )
