from yaml import load, dump, SafeLoader

__all__ = [
    'read_config',
]


def read_config(config_filepath):
    with open(config_filepath, 'r', encoding="utf-8") as f:
        return load(f, Loader=SafeLoader)


def save_config(config_filepath, config):
    with open(config_filepath, 'w', encoding="utf-8") as f:
        dump(config, f)
