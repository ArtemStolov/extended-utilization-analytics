from ext_utilization.model.department import Department
from ext_utilization.model.equipment_class import EquipmentClass
from ext_utilization.model.profession import Profession


class Factory(object):
    def __init__(self):
        self.departments = {}
        self.equipment_classes = {}
        self.professions = {}
        self.employee_variation_id = {}

    def add_department(self, department_id, identity, name, schedule):
        if department_id not in self.departments:
            self.departments[department_id] = Department(department_id, identity, name, schedule)
        return self.departments[department_id]

    def add_equipment_class(self, equipment_id, identity, name):
        if equipment_id not in self.equipment_classes:
            self.equipment_classes[equipment_id] = EquipmentClass(equipment_id, identity, name)
        return self.equipment_classes[equipment_id]

    def add_profession(self, profession_id, identity, name):
        if profession_id not in self.professions:
            self.professions[profession_id] = Profession(profession_id, identity, name)
        return self.professions[profession_id]

    def sort_departments(self):
        self.departments = {
            k: v for k, v in sorted(
                self.departments.items(),
                key=lambda x: x[1].identity
            )
        }

    def clean(self):
        self.departments = {}
        self.equipment_classes = {}
        self.professions = {}
