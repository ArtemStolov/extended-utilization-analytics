from ext_utilization.model.equipment_info import EquipmentInfo
from ext_utilization.model.profession_info import ProfessionInfo


class Department(object):
    def __init__(self, department_id, identity, name, schedule):
        self.department_id = department_id
        self.identity = identity
        self.name = name
        self.equipment = {}
        self.profession = {}
        self.schedule = schedule

    def add_equipment(self, equipment, occupy, quantity):
        if equipment is None:
            return
        self.equipment[equipment] = EquipmentInfo(occupy, quantity)

    def add_profession(self, profession, occupy, quantity):
        if profession is None:
            return
        self.profession[profession] = ProfessionInfo(occupy, quantity)

    def sort_equipment(self):
        self.equipment = {
            k: v for k, v in sorted(
                self.equipment.items(),
                key=lambda x: x[1].occupy,
                reverse=True
            )
        }

    def sort_professions(self):
        self.profession = {
            k: v for k, v in sorted(
                self.profession.items(),
                key=lambda x: x[1].occupy,
                reverse=True
            )
        }
