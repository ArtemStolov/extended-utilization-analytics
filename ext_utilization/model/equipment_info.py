from datetime import datetime

from ext_utilization.logic import Base


class EquipmentInfo(Base):
    def __init__(self, occupy, quantity, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.occupy = occupy
        self.quantity = quantity
        self.period_info = []

    def get_analytics(self, date_to):
        work_time_before = 0
        life_time_before = 0
        work_time_total = 0
        for row in self.period_info:
            if datetime.fromisoformat(row['stop_date']) <= date_to:
                work_time_before += row['operation_time']
                work_time_before += row['preparing_time']
                work_time_before += row['setup_time']
                life_time_before += row['life_time'] * self.quantity - row['stand_time']
            work_time_total += row['operation_time']
            work_time_total += row['preparing_time']
            work_time_total += row['setup_time']
        return {
            'ТРУДОЕМКОСТЬ ОСВОЕННАЯ, ЧАС': round(work_time_before),
            'ЗАГРУЗКА ДО КОНЕЧНОЙ ДАТЫ, %': round(work_time_before / life_time_before * 100),
            'НЕДОДЕЛ, ЧАС': round(work_time_total - work_time_before),
            'ЗАГРУЗКА С УЧЕТОМ НЕДОДЕЛОВ, %': round(work_time_total / life_time_before * 100),
        }
