from argparse import ArgumentParser
from logging import DEBUG, INFO, basicConfig
from os import getcwd
from os.path import join

from ext_utilization.config import read_config, save_config
from ext_utilization.io.xlsx_writer import xlsx_write_data
from ext_utilization.logic import UtilizationReporter

__all__ = [
    'make_export',
]


def make_export():
    parser = ArgumentParser(
        description='Инструмент консольного импорта данных в систему IA.'
    )
    parser.add_argument('-c', '--config', required=False,
                        default=join(getcwd(), 'config.yml'))
    parser.add_argument('-d', '--debug', required=False, action='store_true',
                        default=False)

    args = parser.parse_args()

    basicConfig(level=args.debug and DEBUG or INFO)

    config = read_config(args.config)

    with UtilizationReporter.from_config(config) as reporter:
        for each_session, data in reporter.utilization_report(config).items():
            xlsx_write_data(
                xlsx=config['output']['file'].format(str(each_session).zfill(5)),
                worksheet=config['output']['worksheet'],
                data=data
            )
            config['sessions'].append(each_session)

    save_config(args.config, config)


if __name__ == '__main__':
    make_export()
