from setuptools import find_packages, setup

setup(
    name='ext_utilization',
    version='0.1.0a1',
    packages=find_packages(),
    url='',
    license='MIT',
    author='BFG-Soft',
    author_email='saa@bfg.ai',
    description='Various IA web application tools.',
    install_requires=[
        'PyYAML',
        'requests',
    ],
    extras_require={
        'dev': [
            'pip-tools',
        ],
    },
    entry_points={
        'console_scripts': [
            'ext_utilization_analyser = ext_utilization.scripts:make_export'
        ]
    }
)
